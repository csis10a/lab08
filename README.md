## Getting Started

1. Open a terminal, command prompt, or PowerShell and change to your working directory (e.g. U:)
2. Grab a copy of lab08 using: `git clone https://bitbucket.org/csis10a/lab08.git`
3. Follow the instructions at [https://docs.google.com/document/d/1MoePsQVG5DjEQLOeOrhqhtSPg0AkwBAxxzPmu8sonAI/edit?usp=sharing](https://docs.google.com/document/d/1MoePsQVG5DjEQLOeOrhqhtSPg0AkwBAxxzPmu8sonAI/edit?usp=sharing)

__Note:__ If git is not installed on your computer you can download the lab from [https://bitbucket.org/csis10a/lab08/get/master.zip](https://bitbucket.org/csis10a/lab08/get/master.zip)
